FROM debian:jessie
MAINTAINER Jon Lund Steffensen <jonlst@gmail.com>

RUN apt-get update && apt-get install -y ca-certificates-java
